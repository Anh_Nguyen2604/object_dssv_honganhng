var validation = {
    kiemTraRong: function (value, idError, message) {
        if (value.length == 0) {
            document.getElementById(idError).innerText =
                message;
            return false;
        } else {
            document.getElementById(idError).innerText =
                "";
            return true;
        }
    },

    kiemTraDoDai: function (value, idError, message, min, max) {
        if (value.length < min || value.length > max) {
            document.getElementById(idError).innerText =
                message;
            return false;
        } else {
            document.getElementById(idError).innerText =
                "";
            return true;
        }
    },

    kiemTraEmail: function (value, idError, message) {
        const re =
            /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (re.test(value)) {
            document.getElementById(idError).innerText =
            "";
            return true;
        } else {
            document.getElementById(idError).innerText =
            message;
            return false;
    }
     },

     kiemTraSo: function (value, idError, message) {
        var reg = /^[0-9]+$/;
        if (reg.test(value)) {
          document.getElementById(idError).innerHTML = "";
          return true;
        } else {
          document.getElementById(idError).innerHTML = message;
          return false;
        }
      },
    
    kiemTraKhoangDiem: function (value, idError, message, min, max) {
        if (value *1 < min || value*1 > max) {
          document.getElementById(idError).innerHTML = message;
          return false;
        } else {
          document.getElementById(idError).innerHTML = "";
          return true;
        }
      },
    
};


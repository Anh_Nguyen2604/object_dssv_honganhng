const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";

//  chức năng thêm sinh viên
var dssv = [];
// lấy thông tin từ localStorage

var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
if (dssvJson != null) {
  dssv = JSON.parse(dssvJson);
  //  array khi convert thành json sẽ mất function, ta sẽ map lại
  for (var index = 0; index < dssv.length; index++) {
    var sv = dssv[index];
    dssv[index] = new SinhVien(
      sv.ten,
      sv.ma,
      sv.matKhau,
      sv.email,
      sv.toan,
      sv.ly,
      sv.hoa,
    );
  }

  renderDSSV(dssv);
}


function themSV() {
  var newSv = layThongTinTuForm();
  // console.log("newSv: ", newSv);

  var isValid = validation.kiemTraRong (newSv.ma, "spanMaSV", "Ma sv k dc de trong") 
  && validation.kiemTraDoDai (newSv.ma, "spanMaSV", "Ma sv phai gom 4 ki tu", 4, 4);

  var isValid = isValid & validation.kiemTraRong (newSv.ten, "spanTenSV", "Ten sv ko dc de rong");
  


  var isValid = isValid & validation.kiemTraRong (newSv.matKhau, "spanMatKhau", "MK ko dc de rong");

  var isValid = isValid  & validation.kiemTraRong (newSv.email, "spanEmailSV", "Email ko dc de rong")
  && validation.kiemTraEmail(newSv.email, "spanEmailSV", "Email khong dung format" );

  var isValid = isValid & validation.kiemTraRong (newSv.toan, "spanToan", "Toan ko dc de rong")
  && validation.kiemTraSo (newSv.toan, "spanToan", "Diem toan phai la so")
  && validation.kiemTraKhoangDiem (newSv.toan, "spanToan", "Diem toan phai la so tu 0-10", 0, 10);

  var isValid = isValid & validation.kiemTraRong (newSv.ly, "spanLy", "LY ko dc de rong")
  && validation.kiemTraSo(newSv.ly, "spanLy", "Diem ly phai la so")
  && validation.kiemTraKhoangDiem(newSv.ly, "spanLy", "Diem ly phai la so tu 0-10", 0, 10);

  var isValid = isValid & validation.kiemTraRong (newSv.hoa, "spanHoa", "Hoa ko dc de rong")
  && validation.kiemTraSo(newSv.hoa, "spanHoa", "Diem hoa phai la so")
  && validation.kiemTraKhoangDiem(newSv.hoa, "spanHoa", "Diem hoa phai la so tu 0-10", 0, 10);
  

    if (isValid) {
    dssv.push(newSv);

    // tạo json
    var dssvJson = JSON.stringify(dssv);
    // console.log("dssvJson: ", dssvJson);
    // lưu json vào localStorage
    localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
    renderDSSV(dssv);
    
      
  }

}

function xoaSinhVien(id) {  // hong cai butto
  console.log(id);

  // var index = dssv.findIndex(function (sv) {
  //   return sv.ma == id;
  // });
  var index = timKiemViTri(id, dssv);
  // tìm thấy vị trí
  if (index != -1) {
    dssv.splice(index, 1);
    renderDSSV(dssv);
  }
}

function suaSinhVien(id) {
  var index = timKiemViTri(id, dssv);
  console.log("index: ", index);
  if (index != -1) {
    var sv = dssv[index]; 
    showThongTinLenForm(sv);
    document.getElementById("txtMaSV").disabled = true;

  }
}
function capNhatSV() {
  var oldSV = layThongTinTuForm();
  var index = timKiemViTri(oldSV.ma, dssv);
    dssv[index] = oldSV;
    var dssvJson = JSON.stringify(dssv);
    localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
    renderDSSV(dssv);
}
function resetSV() {
  // localStorage.clear();
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtPass").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
  document.getElementById("txtMaSV").disabled = false;
}